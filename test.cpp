#include <stdio.h>
#include "mpi.h"

int main(int argc, char ** argv) {
   int myid;
    MPI_Init (&argc, &argv);
     MPI_Comm_rank (MPI_COMM_WORLD, &myid);

      if (myid == 0) {
          printf ("\nI am the master\n");
           }
       else {
           printf ("\nI am the slave with process with id %d\n", myid);
            }

        MPI_Finalize ();
        return 0;
}
