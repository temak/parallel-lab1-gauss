#include <stdio.h>
#include <cstdlib>
#include <mpi.h>
#include <vector>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <cmath>
#include <time.h>

using namespace std;
namespace gauss {

const int MASTER_ID = 0;
int myId;   
int N;
int Q2, r2;
int Q3, r3;
double mainElement;
MPI_Request requestSend, requestRecv;
MPI_Status status;
vector<double> myChunk; 
vector<double> u;

double* generateMatrix(int n) {
    double* matr = new double[n * (n + 1)];
    
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            matr[i * (n+1) + j] = (2*i + j) / 100000.0;
        }
        matr[i * (n+1) + i] = 100.0;
        matr[i * (n + 1) + n] = 0;
        for (int j = 0; j < n; ++ j) {
            matr[i * (n+1) + n] += matr[i * (n+1) + j];
        }
    }
    return matr;
}

void printMatr(int n, double* matr) {
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n + 1; ++j) {
            //cout << matr[i * (n+1) + j] << " ";
            printf("%.8f ", matr[i * (n+1) + j]);
        }
        printf("\n");
    }
}


/*
 * iGl == proc_ID
 */
void tile(int iGl, int jGl) {
    //printf("tile(%d, %d)\n", iGl, jGl);

    int jMax = std::min((jGl + 1) * r3, N + 1);

    for (int k = 0; k < N; ++k) {
        int jMin = std::max(k + 1, jGl * r3);

        if (iGl > 0 && k < iGl * r2) {
            //printf("[%d] tile(%d, %d) Recv k =%d \n", myId, iGl, jGl, k);
            MPI_Irecv(&u[jGl * r3], jMax - jGl * r3, MPI_DOUBLE, iGl - 1, jGl, 
                    MPI_COMM_WORLD, &requestRecv);
            MPI_Wait(&requestRecv, &status);
            // printf("[%d] tile(%d, %d) Recv k =%d ok\n", myId, iGl, jGl, k);

            if (iGl < Q2 - 1) {
                //printf("[%d] tile(%d, %d) Send k =%d \n", myId, iGl, jGl, k);
                MPI_Isend(&u[jGl * r3], jMax - jGl * r3, 
                         MPI_DOUBLE, iGl + 1, jGl, MPI_COMM_WORLD, &requestSend);
                // MPI_Wait(&requestSend, &status);
            }

        }
        for (int i = 0; i < r2; ++i) {
            if (i + iGl * r2 < k)
                continue;
            if (i + iGl * r2 == k) {
                
                if (jGl == 0) {
                    mainElement = myChunk[i * (N + 1) + k]; // a[k][k]
                    // printf("[%d] tile(%d, %d) k =%d mainelem: myChunk[%d] = %f ; jMin = %d \n", myId, iGl, jGl, k, i * (N + 1) + k, mainElement, jMin);
                }
                for (int j = jMin; j < jMax; ++j) 
                {
                    myChunk[i * (N + 1) + j] = 
                        u[j] = myChunk[i * (N + 1) + j] / mainElement;
                    // printf("[%d] myChunk[%d] = %f\n", myId, i * (N + 1) + j, myChunk[i * (N + 1) + j]);    
                    
                }
                if (iGl < Q2 - 1) {
                    // printf("[%d] tile(%d, %d) calc_u Send k =%d \n", myId, iGl, jGl, k);
                    MPI_Isend(&u[jGl * r3], jMax - jGl * r3, 
                             MPI_DOUBLE, iGl + 1, jGl, MPI_COMM_WORLD, &requestSend);
                    // MPI_Wait(&requestSend, &status);
                }
                
            } else {
                for (int j = jMin; j < jMax; ++j) 
                {
                    // printf("[%d] tile(%d, %d) k =%d curI = %d, jMin = %d jMax = %d j = %d\n", myId, iGl, jGl, k, i + iGl * r2, jMin, jMax, j);
                    myChunk[i * (N + 1) + j] = myChunk[i * (N + 1) + j] - 
                                               myChunk[i * (N + 1) + k] * u[j];
                    // printf("[%d] myChunk[%d] = %f\n", myId, i * (N + 1) + j, myChunk[i * (N + 1) + j]);
                }
            }
        }
    }
}

}; // namespace gauss

int main(int argc, char ** argv) {
    using namespace gauss;

    int procCount;
    MPI_Init (&argc, &argv);
    MPI_Comm_rank (MPI_COMM_WORLD, &myId);
    MPI_Comm_size(MPI_COMM_WORLD, &procCount);
    
//    if (argc < 3) {
//        cout << "Give me size of the matrix! and Q2" << endl;
//        return 0;
//    }
    

    if (argc < 2) {
        N = 3;
    } else {
        N = atoi(argv[1]);
    }

    Q2 = procCount;
    r2 = ceil( ((double)N) / Q2);
    
    if (argc < 3) {
        r3 = 2;
    } else {
        r3 = atoi(argv[2]);
    }

    Q3 = ceil( (double)(N + 1)/ r3);

    if (myId == MASTER_ID) {
        printf("N = %d\nQ2 = %d\nr2 = %d\nQ3 = %d\nr3 = %d\n",
                N, Q2, r2, Q3, r3);
    }
    
    
    size_t chunkSize = r2 * (N + 1);
    size_t myChunkSize = min(chunkSize, N * (N + 1) - myId * chunkSize);
    
    myChunk.assign(myChunkSize, 0);
    u.assign(N+1, 0);
    
    //double a[] = {1,2,3,6, 2,1,3,6, 3,2,1,6};
    double* matr = NULL;
    double starttime;
    if (MASTER_ID == myId) {

       starttime = MPI_Wtime();
        //printf ("\nI am the master\n");
        
        matr = generateMatrix(N);
        //matr = a;
        //printMatr(N, matr);
        // TODO use memcpy
        for (int i = 0; i < chunkSize; ++i) {
            myChunk[i] = matr[i];
        }
        for (int i = 1; i < Q2; ++i) {    
            MPI_Isend(matr + i * chunkSize, min(chunkSize, N * (N + 1) - i * chunkSize), 
                     MPI_DOUBLE, i, 0, MPI_COMM_WORLD, &requestSend);
        }
        
    }
    else {
       // printf ("\nI am the slave with process with id %d\n", myId);
        MPI_Irecv(&myChunk[0], myChunkSize, MPI_DOUBLE, 0, MPI_ANY_TAG, 
                 MPI_COMM_WORLD, &requestRecv);
        MPI_Wait(&requestRecv, &status);
    }
    
    for (int jGl = 0; jGl < Q3; ++jGl) {
        tile(myId, jGl);
    }

    if (MASTER_ID == myId) {
        printf("Collecting data by master...\n");
        memcpy(matr, &myChunk[0], myChunkSize * sizeof(double));
        MPI_Request* requests = new MPI_Request[Q2 - 1];
        MPI_Status* statuses = new MPI_Status[Q2 - 1];
        for (int i = 1; i < Q2; ++i) {
            MPI_Irecv(matr + i * r2 * (N + 1), 
                     std::min(chunkSize, N * (N + 1) - i * chunkSize),  
                     MPI_DOUBLE, i, MPI_ANY_TAG, MPI_COMM_WORLD, &requests[i-1]);
        }

        MPI_Waitall(Q2 - 1, requests, statuses);
        
        printf("Result after forward:\n");
        // make main diagonal ones and under main diagonal zero
        for (int i = 0; i < N; ++i) {
         for (int j = 0; j <= i; ++j) {
             if (j == i )
                 matr[i * (N + 1) + j] = 1;
             else
                 matr[i * (N + 1) + j] = 0;
         }
        }
        //printMatr(N, matr);

    } else {
        printf("[%d] Sending result\n", myId);
        // std::ostream_iterator<double> out_it (std::cout," | ");
        // std::copy ( myChunk.begin(), myChunk.end(), out_it );
        // cout << endl;
        MPI_Isend(&myChunk[0], myChunkSize, MPI_DOUBLE, MASTER_ID, 0, 
                  MPI_COMM_WORLD, &requestSend);
    }
    
    //float forwTime = float( clock () - begin_time ) /  CLOCKS_PER_SEC;

    double* x = new double[N];
    if(MASTER_ID == myId) {
        x[N - 1] = matr[N * (N + 1) - 1] / matr[N * (N + 1) - 2];
        for(int i = N - 2; i >= 0; --i) {
            x[i] = matr[i * (N + 1) + N];
            for(int j = N - 1; j > i; --j)
                x[i] -= matr[i * (N + 1) + j] * x[j];
            x[i] /= matr[i * (N + 2)];
        }

        //for(int i = 0; i < N; ++i)
            //printf("%.8f ", x[i]);
        //    cout << 'X' << i << '=' << x[i] << endl; 
        //float total = float( clock () - begin_time ) /  CLOCKS_PER_SEC;
        double endtime   = MPI_Wtime(); 
        ofstream out("result.txt", ofstream::app);
        out << N << ' ' << procCount << ' ' << r3 << ' ' << endtime - starttime << endl;
    }

    
    //MPI_Barrier(MPI_COMM_WORLD);

    MPI_Finalize ();
    
    return 0;
}